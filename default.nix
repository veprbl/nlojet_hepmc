with import <nixpkgs> {};

{ rivet }:

stdenv.mkDerivation {
  name = "nlojet++_hepmc";
  srcs = [
    ./cmdline.h
    ./Makefile
    ./nlojet++_hepmc.cc
    ./pdf.h
  ];

  buildInputs = [ boost fastjet gsl hepmc lhapdf nlojet rivet ];
  BOOST_ROOT = boost.dev;

  unpackPhase = ''
    for file in $srcs; do
      ln -s $file $(stripHash "$file")
    done
  '';

  installPhase = ''
    mkdir -p $out/lib
    cp -r .libs $out/lib
    cp nlojet++_hepmc.la $out/lib
    cp .libs/nlojet++_hepmc.so $out/lib
  '';

  setupHook = ./setup-hook.sh;
}
