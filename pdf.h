// vim:et sw=3 sts=3

#ifndef __PDF
#define __PDF

#include <string>
#include <memory>
#include <cmath>

#include <bits/hhc-process.h>

#include <LHAPDF/LHAPDF.h>

template<int BEAM1, int BEAM2>
class lhapdf : public nlo::pdf_and_coupling_hhc
{
public:
   explicit lhapdf(std::string setname, int member)
      : _pdf(LHAPDF::mkPDF(setname, member))
   {};

   double alpha_qcd(unsigned int, double mr2)
   {
      return _pdf->alphasQ2(mr2) / 2 / M_PI;
   };

   void hadronA(double x, double Q2, unsigned int nu, unsigned int nd, double *f)
   {
      load(BEAM1, x, Q2, f);
   };

   void hadronB(double x, double Q2, unsigned int nu, unsigned int nd, double *f)
   {
      load(BEAM2, x, Q2, f);
   };

private:
   std::unique_ptr<LHAPDF::PDF> _pdf;

   void load(int had_id, double x, double Q2, double *f)
   {
      for(int i = 1; i <= 6; i++)
      {
         int iq, ia;
         if (had_id == 2212)
         {
            iq = i;
         }
         else if (had_id == -2212)
         {
            iq = -i;
         }
         else
         {
            throw;
         }
         ia = -iq;
         f[i] = _pdf->xfxQ2(iq, x, Q2) / x;
         f[-i] = _pdf->xfxQ2(ia, x, Q2) / x;
      }
      f[0] = _pdf->xfxQ2(21, x, Q2) / x;
   }
};

#endif
