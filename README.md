nlojet\_hepmc provides a bridge between NLOJet++ and Rivet.

Usage
=====

You will need the following dependencies installed: HepMC, LHAPDF6, FastJet, Rivet, Boost

Compilation is done by just doing ```make```.

You will also need to help libtool find the user module.
On Linux:
```
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:`pwd`/.libs
```
On OSX:
```
export DYLD_LIBRARY_PATH=$DYLD_LIBRARY_PATH:`pwd`/.libs
```
(assumes ``` `pwd` ``` is the nlojet\_hepmc directory)

To run the calculation do:
```
nlojet++ --calculate -cfull -u nlojet++_hepmc.la --max-event 10000000 --run_name=myrun --sqrts=510 --analysis=MC_XS
```
