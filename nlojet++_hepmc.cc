// vim:et sw=3 sts=3

#include <vector>
#include <memory>

#include <cmath>
template<typename T> bool finite(T v) { return std::isfinite(v); }
#include <bits/hhc-phasespace.h>
#include <bits/hhc-jetfunc.h>

#include <Rivet/AnalysisHandler.hh>
#include <HepMC/GenEvent.h>

#include "fastjet/D0RunIIConePlugin.hh"

#include "cmdline.h"
#include "pdf.h"

using std::vector;
using std::string;
using namespace nlo;
using namespace HepMC;

const int BEAM1 = 2212; // PDG code
const int BEAM2 = 2212;
const double HC2 = 389385730; // (\hbar c)^2 pb*GeV^2
const double JET_R = 0.6;

class UserHHC : public user0d_hhc
{
public:
   string run_name;
   Rivet::AnalysisHandler *rivet; // we are not looking to call destructor for this due to interference with Rivet::ProjectionHandler::removeProjectionApplier
   lhapdf<BEAM1, BEAM2> pdf;
   double sqrt_s;
   ScaleVar scale_var;
   double xren2, xfac2;
   double sum_weights;
   unsigned long long num_events;
   fastjet::JetDefinition jetdef;
   UserHHC(string _run_name, string pdf_name, int pdf_member, double _sqrt_s, ScaleVar _scale_var, double xren, double xfac, const std::vector<std::string> &ana_list)
      : run_name(_run_name)
      , rivet(new Rivet::AnalysisHandler())
      , pdf(pdf_name, pdf_member)
      , sqrt_s(_sqrt_s)
      , scale_var(_scale_var)
      , xren2(xren * xren)
      , xfac2(xfac * xfac)
      , sum_weights(0.0)
      , num_events(0)
      , jetdef(fastjet::antikt_algorithm, JET_R)
   {
      for(const string &ana : ana_list) {
         rivet->addAnalysis(ana);
      }
   };

   virtual ~UserHHC()
   {
      rivet->setCrossSection(sum_weights / num_events, 0.);
      rivet->finalize();
      rivet->writeData(run_name + ".yoda");
   };

   void initfunc(unsigned int) override {};
   void userfunc(const event_hhc& evt, const amplitude_hhc& amp) override
   {
      vector<fastjet::PseudoJet> partons;
      const int num_jet = evt.upper();
      for(int i = 1; i <= num_jet; i++)
      {
         partons.push_back(fastjet::PseudoJet(evt[i].X(), evt[i].Y(), evt[i].Z(), evt[i].T()));
      }
      fastjet::ClusterSequence clust_seq(partons, jetdef);
      vector<fastjet::PseudoJet> inclusive_jets = fastjet::sorted_by_pt(clust_seq.inclusive_jets());

      if (inclusive_jets.size() < 1) return;
      double mu;
      if (scale_var == ScaleVar::mjj) {
         mu = (inclusive_jets[0] + inclusive_jets[1]).m();
      } else if (scale_var == ScaleVar::maxpt) {
         mu = inclusive_jets[0].pt();
      } else if (scale_var == ScaleVar::ht) {
         mu = 0;
         for (auto &jet : inclusive_jets) {
            mu += jet.pt();
         }
      } else {
         std::abort();
      }
      if (mu < 5/*GeV*/) return;
      double mu2 = mu * mu;
      weight_hhc weight_vec = amp(pdf, mu2 * xren2, mu2 * xfac2, HC2);
      double weight = weight_conversion<weight_hhc, double>()(weight_vec);
      sum_weights += weight;
      double x1 = evt[-1].Z() / evt[hadron(-1)].Z();
      double x2 = evt[0].Z() / evt[hadron(0)].Z();

      GenEvent *evt_out = new GenEvent( 20, 1 );
      evt_out->weights().push_back(weight);
      HepMC::PdfInfo pdf_info(0, 0, x1, x2, NAN, NAN, NAN);
      evt_out->set_pdf_info(pdf_info);

      evt_out->use_units(HepMC::Units::GEV, HepMC::Units::MM);
      GenVertex* v = new GenVertex();
      GenParticle* p1 = new GenParticle(FourVector(0, 0, +sqrt_s/2, sqrt_s/2), BEAM1, 3);
      GenParticle* p2 = new GenParticle(FourVector(0, 0, -sqrt_s/2, sqrt_s/2), BEAM2, 3);
      v->add_particle_in(p1);
      v->add_particle_in(p2);
      evt_out->set_beam_particles(p1, p2);
      for(int i = 1; i <= num_jet; i++)
      {
         GenParticle* p = new GenParticle(FourVector(evt[i].X(), evt[i].Y(), evt[i].Z(), evt[i].T()), 21, 1);
         v->add_particle_out(p);
      }
      evt_out->set_signal_process_vertex(v);
      //evt_out->print();

      evt_out->set_event_number(num_events);
      rivet->analyze(*evt_out);
      delete evt_out;
   };

   void end_of_event() override
   {
      num_events++;
   }
};

static std::unique_ptr<UserHHC> user_hhc;

void inputfunc(unsigned int& nj, unsigned int& nu, unsigned int& nd)
{

   try {
     po::variables_map vm = handle_cmd_line_options();
     //  number of jets
     nj = vm["njets"].as<unsigned int>();
   } catch(const std::exception &e) {
      std::cerr << e.what() << std::endl;
      std::terminate();
   }

   //  number of the up and down type flavours
   nu = 2U;
   nd = 3U;
}

void psinput(phasespace_hhc *ps, double& s)
{
   double sqrt_s;
   try {
      po::variables_map vm = handle_cmd_line_options();
      sqrt_s = vm["sqrts"].as<double>();
   } catch(const std::exception &e) {
      std::cerr << e.what() << std::endl;
      std::terminate();
   }

   //  total c.m. energy square
   s = sqrt_s * sqrt_s;

   //   You can use your own phase generator.
   //   Here we use the default.
   ps = 0;
}

user_base_hhc* userfunc()
{
   try {
      po::variables_map vm = handle_cmd_line_options();
      user_hhc.reset(new UserHHC(
               vm["run_name"].as<string>(),
               vm["pdf"].as<string>(),
               vm["member"].as<int>(),
               vm["sqrts"].as<double>(),
               vm["scale_var"].as<ScaleVar>(),
               vm["xren"].as<double>(),
               vm["xfac"].as<double>(),
               vm["analysis"].as< std::vector<std::string> >()
         ));
      return user_hhc.get();
   } catch(const std::exception &e) {
      std::cerr << e.what() << std::endl;
      std::terminate();
   } catch(...) {
      std::cerr << "nlojet_hepmc: Exception in UserHHC constructor!" << std::endl;
      std::terminate();
   }
}

extern "C" {
   struct {
      const char *name;
      void *ptr;
   } user_defined_functions[] = {
      {"procindex", (void*)"hhc"},
      {"inputfunc", (void*)inputfunc},
      {"psinput", (void*)psinput},
      {"userfunc",  (void*)userfunc},
      {0, 0}
   };
}
