// vim:et sw=3 sts=3

#include <vector>
#include <string>

#ifdef __APPLE__
#define WILL_REORDER_ARGV
#include <crt_externs.h>

const char** get_cmd_line(int *argc)
{
   *argc = *_NSGetArgc();
   return const_cast<const char**>(*_NSGetArgv());
}
#elif defined(__linux__)
#include <fstream>
#include <iterator>

std::vector<std::string> _argv; // this is global to keep C-style pointers valid

const char** get_cmd_line(int *argc)
{
   // Load values on first call
   if (_argv.empty()) {
      std::ifstream is("/proc/self/cmdline", std::ios::binary);
      std::string s;
      while(getline(is, s, '\x00'))
      {
         if (s == "") break;
         _argv.push_back(s);
      }
   }
   *argc = _argv.size();
   static const char** argv = new const char*[*argc];
   for(int i = 0; i < *argc; i++)
   {
      argv[i] = _argv[i].c_str();
   }
   return argv;
}
#endif

#include <boost/program_options.hpp>
namespace po = boost::program_options;

enum ScaleVar {
   mjj,
   maxpt,
   ht,
};

std::istream& operator>>(std::istream& in, ScaleVar& scale_var)
{
   std::string scale_var_s;

   in >> scale_var_s;

   if (scale_var_s == "mjj") {
      scale_var = ScaleVar::mjj;
   } else if (scale_var_s == "maxpt") {
      scale_var = ScaleVar::maxpt;
   } else if (scale_var_s == "ht") {
      scale_var = ScaleVar::ht;
   } else {
      in.setstate(std::ios_base::failbit);
   }

   return in;
}

po::variables_map handle_cmd_line_options()
{
   try {
      po::options_description desc("Allowed options");
      desc.add_options()
         ("run_name", po::value<std::string>()->default_value("Rivet"), "run name")
         ("njets", po::value<unsigned int>()->default_value(1), "number of jets")
         ("pdf", po::value<std::string>()->default_value("CT10nlo"), "PDF set name")
         ("sqrts", po::value<double>(), "sqrt(s)")
         ("member", po::value<int>()->default_value(0), "pdf member")
         ("scale_var", po::value<ScaleVar>()->default_value(ScaleVar::mjj), "value used for hard scale")
         ("xren", po::value<double>()->default_value(1.), "renormalization scale")
         ("xfac", po::value<double>()->default_value(1.), "factorization scale")
         ("analysis,a", po::value< std::vector<std::string> >(), "Rivet analyses to use")
         ;

      int argc;
      const char **argv = get_cmd_line(&argc);

#ifdef WILL_REORDER_ARGV
      /*
       * nlojet++ used getopt which reorders argv shuffling
       * options and parameters. It seems like the only platform
       * independent way to prevent that is to force --par=val
       * syntax for command line options.
       *
       */
      std::vector< boost::shared_ptr<po::option_description> > opts = desc.options();
      for (int i = 0; i < argc; i++)
      {
         for(std::vector< boost::shared_ptr<po::option_description> >::iterator it = opts.begin();
             it != opts.end(); it++)
         {
            std::string opt_name = (*it)->long_name();
            if ((std::string(argv[i]).find(opt_name) != std::string::npos)
             && (std::string(argv[i]).find(opt_name + "=") == std::string::npos))
            {
               std::cerr << "Wrong syntax for " << opt_name << "option: "
                         << "Use --" << opt_name << "=..." << std::endl;
               std::terminate();
            }
         }
      }
#endif

      po::variables_map vm;
      po::parsed_options parsed
         = po::command_line_parser(argc, argv).options(desc).allow_unregistered().run();
      po::store(parsed, vm);
      po::notify(vm);

      if (!vm.count("sqrts")) {
        std::cerr << "Missing required option '--sqrts='" << std::endl;
        std::exit(EXIT_FAILURE);
      }

      if (!vm.count("analysis")) {
        std::cerr << "Missing required option '--analysis='" << std::endl;
        std::exit(EXIT_FAILURE);
      }

      return vm;
   } catch (const std::exception &e) {
      std::cerr << e.what() << std::endl;
      std::terminate();
   }
}
