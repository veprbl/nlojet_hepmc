with import <nixpkgs> {};

let
  nlojet_hepmc = import ./default.nix { inherit rivet; };
in

stdenv.mkDerivation {
  name = "nlojet_hepmc-env";
  shellHook = ''
    export BOOST_ROOT=${boost.dev}
    export HEPMC_PREFIX=${hepmc}
  '';
  buildInputs = [
    boost
    fastjet
    gsl
    hepmc
    lhapdf
    nlojet
    nlojet_hepmc
    rivet
    lhapdf.pdf_sets.CT10nlo
  ];
}
