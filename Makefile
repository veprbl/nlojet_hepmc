# This requires BOOST_ROOT and HEPMC_PREFIX set correctly

LHAPDF_CFLAGS=$(shell lhapdf-config --cflags | sed "s/ /,/g")
LHAPDF_LDFLAGS=$(shell lhapdf-config --ldflags | sed "s/ /,/g")
FASTJET_CXXFLAGS=$(shell fastjet-config --cxxflags | sed "s/ /,/g")
FASTJET_LDFLAGS=$(shell fastjet-config --libs --rpath=no --plugins=yes | sed "s/ /,/g")
RIVET_CXXFLAGS="$(shell rivet-config --cxxflags | sed "s/ /,/g") -I$(shell rivet-config --includedir) $(shell gsl-config --cflags) $(shell yoda-config --cppflags)"
RIVET_LDFLAGS=$(shell rivet-config --libs | sed "s/ /,/g")
BOOST_CXXFLAGS=$(BOOST_ROOT)/include
BOOST_LDFLAGS="-lboost_program_options"

%.la: %.cc pdf.h cmdline.h
	make clean || true # Force recompilation
	create-nlojet-user $< -o $(basename $@) -Wx,-Wall,-O2,-std=c++11,-g,-I${HEPMC_PREFIX}/include,${BOOST_CXXFLAGS},${LHAPDF_CFLAGS},${FASTJET_CXXFLAGS},${RIVET_CXXFLAGS} -Wl,-lHepMC,-L${HEPMC_PREFIX}/lib,${LHAPDF_LDFLAGS},${FASTJET_LDFLAGS},${RIVET_LDFLAGS},${BOOST_LDFLAGS}

all: nlojet++_hepmc.la

clean:
	rm -rf .libs .obj
	rm *.la
